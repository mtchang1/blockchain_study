var Web3 = require('web3');
var web3;
if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:9527"));
}

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//取帳號錢包餘額
rl.question("Input account for getBalance: ", (account) => {
    if(account != "") {
        web3.eth.getBalance(account).then(function(balance) {
            //將wei轉成ether
            var res = web3.utils.fromWei(balance, "ether")
            console.log(account +" balance: " + res + " ETH");
        });
    } else {
        console.log("Error input...");
    }
    rl.close();
});
