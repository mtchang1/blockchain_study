var Web3 = require('web3');
var web3;
if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:9527"));
}

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("Input address for check: ", (address) => {
    //檢查是否為錢包地址
    var isAddress = web3.utils.isAddress(address);
    console.log(address + " is correct: " + isAddress);

    rl.close();
});