var Web3 = require('web3');
var web3;
if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://172.24.0.147:9527"));
}

//取當前geth區塊高度
web3.eth.getBlockNumber()
.then(function(result) {
    console.log("Last block: " + result);
});