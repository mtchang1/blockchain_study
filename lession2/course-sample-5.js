var Web3 = require('web3');
var web3;
if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:9527"));
}

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("From account: ", (from) => {
    if(from == "") console.log("Error input...");
    else {
        rl.question("Unlock account password? ", (passwd) => {
            if(passwd == "") console.log("Error input...");
            else {
                rl.question("To account: ", (to) => {
                    if(to == "") console.log("Error input...");
                    else {
                        rl.question("Send amount? ", (amount) => {
                            if(amount == "") console.log("Error input...");
                            else {
                                //將ether轉成wei
                                var wei = web3.utils.toWei(amount, "ether");
                                sendTransaction(from, passwd, to, wei);
                            }
                            rl.close();
                        });
                    }
                });
            }
        });
    }
});

async function sendTransaction(send, password,rece,weiNum) {
    //解鎖轉出錢包帳號
    await web3.eth.personal.unlockAccount(send, password, 300).then(function(res){
        if(res) console.log(send + " unlock success !");
        else console.log(send + " unlock fail !!!");
    });

    var hexData = web3.utils.toHex("我可以紀錄data...");
    //轉帳 on blockchain
    await web3.eth.sendTransaction({
        from: send,
        to: rece,
        value: weiNum,
        data: hexData
    })
    .then(function(receipt) {
        console.log("Send Transaction Success ! Information below ...");
        console.log(receipt);
        console.log('send:');
        findEth(send).then(function(result){
            console.log(result);
        });
        console.log('rec:')
        findEth(rece).then(function(result){
            console.log(result);
        });
    });

    //上鎖轉出錢包帳號
    await web3.eth.personal.lockAccount(send, password).then(function(res) {
        if(res) console.log("Account lock success !");
        else console.log("Account lock fail !!!");
    });
}

async function findEth(publicKey){
    var ethNum;
    await web3.eth.getBalance(publicKey).then(function(res){
        ethNum = web3.utils.fromWei(res, 'ether');
    });
    return ethNum;
}