var Web3 = require('web3');
var web3;
if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:9527"));
}

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//建立新帳號錢包
var account = "";
rl.question("Input the password for creating account:", (passwd) => {
    if(passwd != "") {
        web3.eth.personal.newAccount(passwd).then(function(account) {
            console.log("new accounts: " + account);
        });
    } else {
        console.log("Error input...");
    }
    rl.close();
});
