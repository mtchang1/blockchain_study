pragma solidity ^0.4.0;

contract Calc{
  /*區塊鏈儲存*/
  uint count;

  /*執行會寫入資料，所以需要`transaction`的方式執行。*/
  function add(uint a, uint b) public returns(uint){
    count++;
    return a + b;
  }

  /*執行不會寫入資料，所以允許`call`的方式執行。*/
  function getCount() public constant returns(uint){
    return count;
  }
}
