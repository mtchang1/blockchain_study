var Web3 = require('web3');
var web3;
if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://114.33.201.242:9527"));
}
const fs = require("fs");
const solc = require('solc');

//編譯合約
const input = fs.readFileSync('calc.sol');
const output = solc.compile(input.toString(), 1);
var bytecode;
var abi;

for (var contractName in output.contracts) {
	// code and ABI that are needed by web3
	bytecode = "0x" + output.contracts[contractName].bytecode;
	abi = JSON.parse(output.contracts[contractName].interface);
	// console.log(contractName + ": " + output.contracts[contractName].bytecode);
	// console.log(contractName + ": " + JSON.parse(output.contracts[contractName].interface));
	// console.log(JSON.stringify(abi, undefined, 2));
}

var from = "0xe235b5c5610c679a2d03926cbc5b47e777f89f94";
var passwd = "0-opklm,";

//取合約實例
const contract = new web3.eth.Contract(abi);
contractDeploy(from, passwd);

async function contractDeploy(fromAddr, password) {
	//解鎖錢包帳號
	await web3.eth.personal.unlockAccount(fromAddr, password, 300).then(function(res){
		if(res) console.log(fromAddr + " unlock success !");
		else console.log(fromAddr + " unlock fail !!!");
	});

	//部署合約
	await contract.deploy({
		data: bytecode,
		arguments: []
	}).send({
		from: fromAddr,
		gas: 3000000,
		gasPrice: '1000000000',
	})
	.then((instance) => {
		console.log('Address: ' + instance.options.address);
		console.log('ABI: ' + JSON.stringify(abi, undefined, 2));
	});

	//上鎖錢包帳號
	await web3.eth.personal.lockAccount(fromAddr, password).then(function(res) {
		if(res) console.log(fromAddr + " lock success !");
		else console.log(fromAddr + " lock fail !!!");
	});
}