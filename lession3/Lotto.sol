pragma solidity ^0.4.19;
contract Lotto {
 
    // CONSTANT
    uint256 private maxTickets;
    uint256 public ticketPrice; 
     
    // LOTO REGISTER
    uint256 public lottoIndex;
    uint256 lastTicketTime;
    
    // LOTTO VARIABLES
    uint256 numtickets;
    uint256 totalBounty;
    
    address worldOwner;   
     
    event NewTicket(address indexed fromAddress, bool success);
    event LottoComplete(address indexed fromAddress, uint indexed lottoIndex, uint256 reward);
    
    // Create a new Lotto
    function Lotto() public {
        worldOwner = msg.sender; 
        
        ticketPrice = 0.01 * 10**18;
        maxTickets = 1 + random();
        
        lottoIndex = 1;
        lastTicketTime = 0;
        
        numtickets = 0;
        totalBounty = 0;
    }

    // get now balance
    function getBalance() public view returns (uint256 balance) {
        uint256 rtn_balance = 0;
        
        if(worldOwner == msg.sender) rtn_balance = this.balance;
        
        return rtn_balance;
    }
    
    
	function withdraw() public {
        require(worldOwner == msg.sender);  
        
		// reset for next round
        lottoIndex += 1;
        numtickets = 0;
        totalBounty = 0;
        // change max tickets to give unpredictability
		maxTickets = 1 + random();
		
		worldOwner.transfer(this.balance); 
    }
    
    
    function getLastTicketTime() public view returns (uint256 time) {
        time = lastTicketTime; 
        return time;
    }
    
	// buy the game ticket
    function buyTicket() public payable {
        require(msg.value == ticketPrice); 
        require(numtickets < maxTickets);
        
		// update
		lastTicketTime = now;
        numtickets += 1;
        totalBounty += ticketPrice;
        bool success = numtickets == maxTickets;
		
        NewTicket(msg.sender, success);
        
		// check if winner
        if(success) 
        {
            payWinner(msg.sender);
        } 
    }
    
    
    function payWinner(address winner) private { 
        require(numtickets == maxTickets);
        
		// calc reward
        uint256 ownerTax = 5 * totalBounty / 100;
        uint256 winnerPrice = totalBounty - ownerTax;
        
        LottoComplete(msg.sender, lottoIndex, winnerPrice);
         
		// reset for next round
        lottoIndex += 1;
        numtickets = 0;
        totalBounty = 0;
		
		// change max tickets to give unpredictability
		maxTickets = 1 + random();
         
		// give real money
        worldOwner.transfer(ownerTax);
        winner.transfer(winnerPrice); 
    }
    
    function random() private view returns (uint8) {
        return uint8(uint256(keccak256(block.timestamp, block.difficulty)) % 4);
    }
}