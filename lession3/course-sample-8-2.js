var Web3 = require('web3');
var web3;
if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://114.33.201.242:9527"));
}

var CalcABI = [
	{
		"constant": false,
		"inputs": [
			{
				"name": "a",
				"type": "uint256"
			},
			{
				"name": "b",
				"type": "uint256"
			}
		],
		"name": "add",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "getCount",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
];
var myContract = new web3.eth.Contract(CalcABI, "0xbC2f93515Dd346F8c7F16c983972F6574aA99D75");

const readline = require('readline');
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

var fromAddr = "0xe235b5c5610c679a2d03926cbc5b47e777f89f94";
var passwd = "0-opklm,";
rl.question("Input value of a: ", (a) => {
	if(a == "") console.log("Input Error...");
	else {
		rl.question("Input value of b: ", (b) => {
			if(b == "") console.log("Input Error...");
			else {
				addSend(fromAddr, passwd, a, b);
			}
			rl.close();
		});
	}
});

async function addSend(fromAddr, password, a, b) {
    //解鎖錢包帳號
    // await web3.eth.personal.unlockAccount(fromAddr, password, 300).then(function(res){
    //     if(res) console.log(fromAddr + " unlock success !");
    //     else console.log(fromAddr + " unlock fail !!!");
    // });

	//methods.add.call
	try {
		await myContract.methods.add(a, b).call({from: fromAddr}).then(function(c, err) {
			myContract.methods.getCount().call().then(function(res) {
				console.log("a + b = " + c);
				console.log("getCount return: " + res);
			});
		});
	} catch (e) {
		console.log("Execption: " + e);
	}

    //上鎖錢包帳號
    // await web3.eth.personal.lockAccount(fromAddr, password).then(function(res) {
    //     if(res) console.log(fromAddr + " lock success !");
    //     else console.log(fromAddr + " lock fail !!!");
    // });
}
