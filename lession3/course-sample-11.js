var Web3 = require('web3');
var web3;
if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://114.33.201.242:9527"));
}

var CalcABI = [
	{
		"constant": true,
		"inputs": [],
		"name": "getBalance",
		"outputs": [
			{
				"name": "balance",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "ticketPrice",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "getLastTicketTime",
		"outputs": [
			{
				"name": "time",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "withdraw",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "lottoIndex",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "buyTicket",
		"outputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "fromAddress",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "success",
				"type": "bool"
			}
		],
		"name": "NewTicket",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "fromAddress",
				"type": "address"
			},
			{
				"indexed": true,
				"name": "lottoIndex",
				"type": "uint256"
			},
			{
				"indexed": false,
				"name": "reward",
				"type": "uint256"
			}
		],
		"name": "LottoComplete",
		"type": "event"
	}
];

var myContract = new web3.eth.Contract(CalcABI, "0x578606c6f97eeb2f32fa0c47b0dfff516bfecd77");
const fromAddr = "0xe235B5C5610C679a2D03926cBc5B47e777f89F94";
const passwd = "0-opklm,";

async function addTicket(_fromAddr, _password, _ticketPrice) {
    //解鎖錢包帳號
    await web3.eth.personal.unlockAccount(_fromAddr, _password, 300).then(function(res){
        if(res) console.log(_fromAddr + " unlock success !");
        else console.log(_fromAddr + " unlock fail !!!");
    });

	//methods.buyTicket.send
	try {
		let res = await myContract.methods.buyTicket().send({from: _fromAddr, value: _ticketPrice});
		console.log(res);
	} catch (e) {
		console.log("Execption: " + e);
	}

    //上鎖錢包帳號
    await web3.eth.personal.lockAccount(_fromAddr, _password).then(function(res) {
        if(res) console.log(_fromAddr + " lock success !");
        else console.log(_fromAddr + " lock fail !!!");
    });
}

myContract.methods.ticketPrice().call().then(function(wei_ticketPrice) {
	addTicket(fromAddr, passwd, wei_ticketPrice);
});

