const Web3 = require('web3');
var web3;
if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://114.33.201.242:9527"));
}

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var MyTokenABI = [
	{
		"constant": true,
		"inputs": [],
		"name": "totalSupply",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_owner",
				"type": "address"
			}
		],
		"name": "balanceOf",
		"outputs": [
			{
				"name": "balance",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_to",
				"type": "address"
			},
			{
				"name": "_value",
				"type": "uint256"
			}
		],
		"name": "transfer",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "symbol",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "from",
				"type": "address"
			},
			{
				"indexed": true,
				"name": "to",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "value",
				"type": "uint256"
			}
		],
		"name": "Transfer",
		"type": "event"
	}
]
var myContract = new web3.eth.Contract(MyTokenABI, "0x76013100c6540142ed725e589ff06f3e4c098024");

const fromAddr = "0xe235b5c5610c679a2d03926cbc5b47e777f89f94";
const passwd = "0-opklm,";

async function sendTransaction(send, password, rece, weiNum) {
    //解鎖轉出錢包帳號
    await web3.eth.personal.unlockAccount(send, password, 300).then(function(res){
        if(res) console.log(send + " unlock success !");
        else console.log(send + " unlock fail !!!");
    });

    try {
        //呼叫contract.methods.transfer轉token
        await myContract.methods.transfer(rece, weiNum).send({from: send})
        .then(function(res) {
            console.log(res);
			getBalance(rece).then(function(res) {
                if(res > 0) console.log(rece + " balance: " + res);
            });
        });
    } catch(e) {
        console.log(e);
    }

    //上鎖轉出錢包帳號
    await web3.eth.personal.lockAccount(send, password).then(function(res) {
        if(res) console.log("Account lock success !");
        else console.log("Account lock fail !!!");
    });
}

async function getBalance(address) {
    var balance = 0;
    if(address != "") {
        //呼叫contract.methods.balanceOf查token餘額
        await myContract.methods.balanceOf(address).call(function(err, res) {
            if(err == null) balance = res;
        });
    }
    return balance;
}

rl.question('Transaction to account: ', (toAddr) => {
    if(toAddr == "") console.log("Input Error...");
    else {
        rl.question('Transaction amount? ', (amount) => {
            if(amount == "") console.log("Input Error...");
            else {
                //將ether轉成wei
                var wei = web3.utils.toWei(amount, "ether");
                sendTransaction(fromAddr, passwd, toAddr, wei);
            }
            rl.close();
        });
    }
});